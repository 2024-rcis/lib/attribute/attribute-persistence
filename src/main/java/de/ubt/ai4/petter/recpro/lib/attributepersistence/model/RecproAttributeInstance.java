package de.ubt.ai4.petter.recpro.lib.attributepersistence.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.*;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.util.RestService;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;


import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "attributeType", visible=true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = RecproBinaryAttributeInstance.class, name = "BINARY"),
        @JsonSubTypes.Type(value = RecproTextualAttributeInstance.class, name = "TEXT"),
        @JsonSubTypes.Type(value = RecproNumericAttributeInstance.class, name = "NUMERIC"),
        @JsonSubTypes.Type(value = RecproMetaAttributeInstance.class, name = "META"),
})

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class RecproAttributeInstance implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String recproElementId;
    private String recproElementInstanceId;
    private String recproAttributeId;

    @Enumerated(EnumType.STRING)
    private RecproAttributeType attributeType;
    private String recproProcessInstanceId;

    public static List<RecproAttributeInstance> fromRecproAttributesAndElementId(List<RecproAttribute> attributes, String recproElementId, String recproElementInstanceId, String recproProcessInstanceId) {
        return attributes.stream().map(attribute -> RecproAttributeInstance.fromRecproAttributeAndElementId(attribute, recproElementId, recproElementInstanceId, recproProcessInstanceId)).toList();
    }

    public static RecproAttributeInstance fromRecproAttributeAndElementId(RecproAttribute attribute, String recproElementId, String recproElementInstanceId, String recproProcessInstanceId) {
        return switch (attribute.getAttributeType()) {
            case NUMERIC -> {
                Double object = ((RecproNumericAttribute) attribute).getDefaultValue();
                if (attribute.isFromUrl()) {
                    object = RestService.getDoubleFromUrl(attribute.getUrl());
                }
                yield new RecproNumericAttributeInstance(-1L, recproElementId, recproElementInstanceId, attribute.getId(), attribute.getAttributeType(), object, recproProcessInstanceId);
            }
            case TEXT -> {
                String object = ((RecproTextualAttribute) attribute).getDefaultValue();
                if (attribute.isFromUrl()) {
                    object = RestService.getStringFromUrl(attribute.getUrl());
                }
                yield new RecproTextualAttributeInstance(-1L, recproElementId, recproElementInstanceId, attribute.getId(), attribute.getAttributeType(), object, recproProcessInstanceId);
            }
            case BINARY -> {
                boolean object = ((RecproBinaryAttribute) attribute).isDefaultValue();
                if (attribute.isFromUrl()) {
                    object = RestService.getBooleanFromUrl(attribute.getUrl());
                }
                yield new RecproBinaryAttributeInstance(-1L, recproElementId, recproElementInstanceId, attribute.getId(), attribute.getAttributeType(), object, recproProcessInstanceId);
            }
            default ->  new RecproMetaAttributeInstance(-1L, recproElementId, recproElementInstanceId, attribute.getId(), attribute.getAttributeType(), recproProcessInstanceId);
        };
    }


    public static List<RecproAttributeInstance> copy(List<RecproAttributeInstance> instances) {
        return instances.stream().map(instance -> instance.copy()).toList();
    }

    public RecproAttributeInstance copy() {
        RecproAttributeInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }
}
