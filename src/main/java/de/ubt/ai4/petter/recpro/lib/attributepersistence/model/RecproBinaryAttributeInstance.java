package de.ubt.ai4.petter.recpro.lib.attributepersistence.model;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttributeType;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter


@Entity
@DiscriminatorValue("BINARY")
public class RecproBinaryAttributeInstance extends RecproAttributeInstance {

    public RecproBinaryAttributeInstance(Long id, String recproElementId, String recproElementInstanceId, String recproAttributeId, RecproAttributeType attributeType, boolean value, String recproProcessInstanceId) {
        super(id, recproElementId, recproElementInstanceId,recproAttributeId, attributeType, recproProcessInstanceId);
        this.value = value;
    }
    private boolean value;

}
