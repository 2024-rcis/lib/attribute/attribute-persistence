package de.ubt.ai4.petter.recpro.lib.attributepersistence.model;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttributeType;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter

@Entity
@DiscriminatorValue("META")
public class RecproMetaAttributeInstance extends RecproAttributeInstance {
    public RecproMetaAttributeInstance(Long id, String recproElementId, String recproElementInstanceId, String recproAttributeId, RecproAttributeType attributeType, String recproProcessInstanceId) {
        super(id, recproElementId, recproElementInstanceId,recproAttributeId, attributeType, recproProcessInstanceId);
    }
}
