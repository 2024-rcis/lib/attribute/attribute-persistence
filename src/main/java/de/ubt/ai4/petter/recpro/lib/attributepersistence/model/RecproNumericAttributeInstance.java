package de.ubt.ai4.petter.recpro.lib.attributepersistence.model;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttributeType;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@DiscriminatorValue("NUMERIC")
public class RecproNumericAttributeInstance extends RecproAttributeInstance {

    public RecproNumericAttributeInstance(Long id, String recproElementId, String recproElementInstanceId, String recproAttributeId, RecproAttributeType attributeType, Double value, String recproProcessInstanceId) {
        super(id, recproElementId, recproElementInstanceId,recproAttributeId, attributeType, recproProcessInstanceId);
        this.value = value;
    }
    private double value;
}
