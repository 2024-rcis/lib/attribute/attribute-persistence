package de.ubt.ai4.petter.recpro.lib.attributepersistence.service;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class AttributeInstanceExecutionService {

    private AttributeInstancePersistenceService persistenceService;

    public List<RecproAttributeInstance> getByRecproAttributeInstances(List<RecproAttribute> attributes, String recproElementId, String recproElementInstanceId, String recproProcessInstanceId) {
        return attributes.stream().map(attribute -> persistenceService.getByRecproElementInstanceId(RecproAttributeInstance.fromRecproAttributeAndElementId(attribute, recproElementId, recproElementInstanceId, recproProcessInstanceId))).toList();
    }

    public RecproAttributeInstance getByRecproAttribute(RecproAttribute attribute, String recproElementId, String recproElementInstanceId, String recproProcessInstanceId) {
        return persistenceService.getByRecproElementInstanceId(RecproAttributeInstance.fromRecproAttributeAndElementId(attribute, recproElementId, recproElementInstanceId, recproProcessInstanceId));
    }

    public RecproAttributeInstance save(RecproAttributeInstance instance) {
        return persistenceService.saveAndFlush(instance);
    }
}
