package de.ubt.ai4.petter.recpro.lib.attributepersistence.service;


import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AttributeInstanceRepository extends JpaRepository<RecproAttributeInstance, Long> {
    List<RecproAttributeInstance> findByRecproElementInstanceIdAndRecproAttributeIdAndRecproProcessInstanceIdOrderByIdDesc(String recproElementInstanceId, String recproAttributeId, String processInstanceId);
    List<RecproAttributeInstance> findByRecproAttributeIdAndRecproProcessInstanceIdOrderByIdDesc(String recproAttributeId, String processInstanceId);

    List<RecproAttributeInstance> findAllByRecproProcessInstanceIdIn(List<String> recproProcessInstanceIds);
}
