package de.ubt.ai4.petter.recpro.lib.attributepersistence.util;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

public class RestService {

    public static final Double getDoubleFromUrl(String url) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        return Objects.requireNonNull(template.getForObject(builder.toUriString(), Double.class));
    }

    public static final String getStringFromUrl(String url) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        return Objects.requireNonNull(template.getForObject(builder.toUriString(), String.class));
    }

    public static final Boolean getBooleanFromUrl(String url) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        return Objects.requireNonNull(template.getForObject(builder.toUriString(), Boolean.class));
    }

}
